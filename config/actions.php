<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 13/05/2019
 * Time: 15:42
 */


use kore\Kore;
use \kore\base\utils\KAllert;


Kore::htmlRenderer()->addDir(__DIR__.'/../resources/views');


Kore::noticePospone(new \kore\base\KAction([
    'execute' => function() {
        $driver = Kore::getDriver();

        if (isset(Kore::$params['id'])){
            $notice = $driver->getEntity(Kore::notification(), Kore::$params['id']);
            $type = Kore::notice()->{$notice->type};

            if ($notice == null){
                Kore::allert( new KAllert(
                    'Error: couldn´t find notice ' . Kore::$params['id'],
                    'danger'
                ) );
                return;
            }

            if ($type == null){
                Kore::allert( new KAllert(
                    'Error: couldn´t find notice type ' . $notice->type,
                    'danger'
                ) );
                return;
            }
            
            if (!$notice->canBePosposed){
                Kore::allert( new KAllert(
                    'This notice can´t be posponed',
                    'danger'
                ) );
                return;
            }

            // We set the state to posponed, and the datetime when it is posponed to
            $notice->state = 4;
            $notice->posDatetime = new DateTime();

            $interval = new DateInterval('P' . $type->posposeDays .'D');
            $notice->posDatetime = $notice->posDatetime->add($interval);

            Kore::addTarget($notice);
        }

    }
]));


Kore::noticeAccept(new \kore\base\KAction([
    'execute' => function() {
        $driver = Kore::getDriver();

        if (isset(Kore::$params['id'])){
            $notice = $driver->getEntity(Kore::notification(), Kore::$params['id']);
            $type = Kore::notice()->{$notice->type};

            if ($notice == null){
                Kore::allert( new KAllert(
                    'Error: couldn´t find notice ' . Kore::$params['id'],
                    'danger'
                ) );
                return;
            }

            if ($type == null){
                Kore::allert( new KAllert(
                    'Error: couldn´t find notice type ' . $notice->type,
                    'danger'
                ) );
                return;
            }

            $type->onAcceptCallback(json_decode($notice->data, true));
            
            // We set the state to accepted
            $notice->state = 3;
            Kore::addTarget($notice);
        }
    }
]));


Kore::noticeDissmiss(new \kore\base\KAction([
    'execute' => function() {
        $driver = Kore::getDriver();

        if (isset(Kore::$params['id'])){
            $notice = $driver->getEntity(Kore::notification(), Kore::$params['id']);
            $type = Kore::notice()->{$notice->type};

            if ($notice == null){
                Kore::allert( new KAllert(
                    'Error: couldn´t find notice ' . Kore::$params['id'],
                    'danger'
                ) );
                return;
            }

            if ($type == null){
                Kore::allert( new KAllert(
                    'Error: couldn´t find notice type ' . $notice->type,
                    'danger'
                ) );
                return;
            }

            if (!$notice->canBeDissmised){
                Kore::allert( new KAllert(
                    'This notice can´t be dissmised',
                    'danger'
                ) );
                return;
            }

            $type->onDissmissCallback(json_decode($notice->data, true));

            // We set the state to discarted
            $notice->state = 2;
            Kore::addTarget($notice);
        }

    }
]));


\kore\Kore::htmlRenderer()->noticePospone = function (){
    if (isset(Kore::$params['reAction'])){
        $kModel = null;

        if (isset(Kore::$params['reModel'])){
            $kModel = Kore::$params['reModel'];
        }

        \kore\KEngine::reAction(Kore::$params['reAction'], $kModel);
    }else{
        \kore\KEngine::reAction('table', Kore::auth()->redirectModel);
    }
};

\kore\Kore::htmlRenderer()->noticeAccept = function (){
    if (isset(Kore::$params['reAction'])){
        $kModel = null;

        if (isset(Kore::$params['reModel'])){
            $kModel = Kore::$params['reModel'];
        }

        \kore\KEngine::reAction(Kore::$params['reAction'], $kModel);
    }else{
        \kore\KEngine::reAction('table', Kore::auth()->redirectModel);
    }
};

\kore\Kore::htmlRenderer()->noticeDissmiss = function (){
    if (isset(Kore::$params['reAction'])){
        $kModel = null;

        if (isset(Kore::$params['reModel'])){
            $kModel = Kore::$params['reModel'];
        }

        \kore\KEngine::reAction(Kore::$params['reAction'], $kModel);
    }else{
        \kore\KEngine::reAction('table', Kore::auth()->redirectModel);
    }
};