<?php


use kore\Kore;
use kore\base\KType;
use kore\base\KTree;


// Tipo de objeto password; simplemente hashea la contraseña automáticamente, y no devuelve valor alguno
Kore::kPass(Kore::kString()->extend([
    'parse' => function ($val) {
        if (strlen($val) == 64){
            return $val;
        }
        return hash('sha256', $val);
    },
    'hash' => function ($val) {
        return hash('sha256', $val);
    }
]));
