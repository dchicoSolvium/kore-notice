<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */

include_once 'types.php';

use kore\Kore;
use kore\base\KModel;
use kore\base\KField;

// Usuarios

Kore::notification(new KModel([]));


Kore::notification()->id = new KField([], 'kId');

Kore::notification()->authUser = new KField([
    'label' => 'Usuario',
    'required' => true,
    'target' => 'authUser',
    'string' => '%username%'
], 'kFK');


Kore::notification()->datetime = new KField([
    'label' => 'Fecha y hora',
    'required' => true,
], 'kDatetime');


Kore::notification()->posDatetime = new KField([
    'label' => 'Fecha y hora postpuestas',
    'required' => false,
    'nullable' => true
], 'kDatetime');


Kore::notification()->state = new KField([
    'label' => 'Estado',
    'options' => [
        '1' => 'En espera',
        '2' => 'Descartada',
        '3' => 'Aceptada',
        '4' => 'Pospuesta'
    ],
    'required' => true
], 'kSelect');


Kore::notification()->type = new KField([
    'label' => 'Tipo',
    'options' => [
    ],
    'required' => false
], 'kSelect');


Kore::notification()->data = new KField([
    'label' => 'Datos',
    'required' => false,
    'nullable' => true
], 'kText');

Kore::notification()->message = new KField([
    'label' => 'Mensaje',
    'required' => false
], 'kText');

Kore::notification()->canBeDissmised = new KField([
    'label' => 'Se puede cancelar',
    'required' => false
], 'kBool');

Kore::notification()->canBePosposed = new KField([
    'label' => 'Se puede posponer',
    'required' => false
], 'kBool');


Kore::notification()->title = 'Notificaciones';

Kore::notification()->actions = [
    ['desc' => 'aceptar', 'action' => 'noticeAccept'],
    ['desc' => 'posponer', 'action' => 'noticePospone'],
    ['desc' => 'rechazar', 'action' => 'noticeDissmiss']];

Kore::comercial()->actions[] = ['desc' => 'empresas', 'action' => 'empresa/-1/table'];

Kore::notification()->tableFields = ['authUser', 'datetime', 'state', 'message'];
Kore::notification()->editFields = ['authUser', 'datetime', 'state', 'message'];
Kore::notification()->createFields = ['authUser', 'type', 'datetime', 'state', 'message'];

Kore::notification()->typeRule = new \kore\base\KRule(function ($notification) {
    $type = Kore::notice()->{$notification->type};

    if ($type == null) {
        return false;
    }

    var_dump($type->canBePosposed); die();
    $notification->canBePosposed = $type->canBePosposed == true;
    $notification->state = '1';
    $notification->datetime = new DateTime();
    $notification->canBePosposed = $type->canBePosposed;
    $notification->canBeDissmised = $type->onDissmissCallback != null;

    $notification->message = $type->createMessage($notification->data);

    return true;
    
}, 'Notification type unregistered', [], \kore\base\KRule::KINSERT);


Kore::notification()->getData = function ($notice){
    return json_decode(Kore::kSelf()->data);
};

Kore::notification()->setData = function ($notice, $data){
    $self = $notice;

    $self->data = json_encode($data);
};

Kore::notification()->getMessage = function ($notice){
    $self = $notice;
    $type = Kore::notice()->{$notification->type};

    $data = Kore::notification()->getData($self);

    return $type->createMessage($data);
};


