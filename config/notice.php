<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */

use kore\Kore;

include_once 'models.php';


//class KNoticeType extends \kore\base\KObject{
//    /**
//     * @var function(array $data)
//     */
//    public $onAcceptCallback = null;
//
//    /**
//     * @var function(array $data)
//     */
//    public $onDissmissCallback = null;
//
//    /**
//     * @var function(array $data)
//     */
//    public $createMessage = null;
//
//    public $canBePosposed = true;
//
//    public $posposeDays = true;
//
//    public $description = '';
//}

Kore::notice(new \kore\base\Komponent());

/**
 * Función que añade un tipo de notificación al kore. Se añade también en las opciones
 * del campo type del modelo notificación
 * 
 * $description puede incluir valores insertados en el $data de una notificación nueva.
 * esto es, cada notificación tiene un tipo cuya descripción será generada automáticamente
 * a partir de $description y el método createMessage, que por defecto será el que recoje
 * la descripción y sustituye los valores insertados con %% por sus correspondientes
 * valores en $data
 * 
 * @param $name
 * @param $description
 * @param null $onAcceptCallback
 * @param null $onDissmissCallback
 */
Kore::notice()->addNoticeType = function ($name, $description, $text, $onAcceptCallback = null, $onDissmissCallback = null, $canBePosposed = true){
    Kore::notice()->$name = new \kore\base\KObject([], $name);

    Kore::notice()->$name->onAcceptCallback = $onAcceptCallback;
    Kore::notice()->$name->onDissmissCallback = $onDissmissCallback;
    Kore::notice()->$name->canBePosposed = $canBePosposed;
    Kore::notice()->$name->posposeDays = 1;

    Kore::notice()->$name->text = $text;

    // Función por defecto para crear un mensaje
    Kore::notice()->$name->createMessage = function($data){
        $msg = Kore::kSelf()->text;

        if (!isset($data)){
            return $msg;
        }

        foreach ($data as $k => $v){
            if ($v instanceof \kore\base\Entity){
                foreach ($v->getFields() as $field){
                    $fname = $field->getName();
                    $msg = str_replace("%$k.$fname%", $v->$fname, $msg);
                }
            }
            
            $msg = str_replace("%$k%", $v, $msg);
        }
        
        return $msg;
    };

    $options = Kore::notification()->type->options;
    $options[$name] = $description;

    Kore::notification()->type->options = $options;
};


Kore::notice()->checkPosposedNotifications = function (){
    $driver = Kore::getDriver();

    try{
        $posposed = $driver->list(Kore::notification(),[
            'state' => '4',
            'posDatetime' => ['<=', new DateTime()]
        ]);
    }catch (Exception $e){
        $posposed = [];
    }

    
    foreach ($posposed as $pp){
        $pp->state = '1';
        $pp->posDatetime = null;

        Kore::addTarget($pp);
    }
};

Kore::notice()->preprocess = function (){
    Kore::kSelf()->checkPosposedNotifications();
};

Kore::notice()->postprocess = function (){
    $user = Kore::auth()->getUser();
    
    if ($user == null){
        return;
    }

    Kore::$params['notice'] = Kore::getDriver()->list(Kore::notification(), [
        'authUser' => $user->id,
        'state' => '1'
    ]);
};
