<?php

namespace kore\notice;

use kore\KEngine;
use kore\Kore;
use kore\KPlugin;

/**
 * Created by David Chico.
 * Mail: davidchico@solvium.es
 * Date: 26/03/2019
 */
class KNotice extends KPlugin
{

    public $config = [__DIR__.'/../../../config'];

    public function install()
    {
    }

}